#!/usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.


def kickstart_c7(subnet):
    """Generates a kickstart for Centos 7 install"""
    tree = 'http://mirror.centos.org/centos/7/os/x86_64/'
    ks_initial = 'install\ncmdline\nshutdown\nignoredisk --only-use=vda\nskipx\n'
    ks_repo = 'url --url="' + tree + '"\n'
    ks_kb = 'keyboard us\n'
    ks_lang = 'lang en_US.UTF-8\n'
    ks_tz = 'timezone --utc America/New_York\n'
    ks_authconfig = 'authconfig --enableshadow --passalgo=sha512\n'
    ks_rootpw = 'rootpw --plaintext bacon\n'
    ks_security = 'firewall --service=ssh\nselinux --enforcing\n'
    ks_bootloader = 'bootloader --location=mbr --boot-drive=vda\n'
    ks_disk_clear = 'zerombr\nclearpart --all --initlabel --drives=vda\n'
    ks_part = 'autopart --type=plain\n'
    ks_packages = '%packages\n@core\nsysstat\nqemu-guest-agent\n'
    ks_post = '%post\n'
    ks_sched = (
        'grubby --update-kernel=ALL --args="elevator=noop net.ifnames=0 biosdevname=0"\n')
    ks_end = '%end\n'

    # Generate dynamic bits
    kickstart = ks_initial + ks_repo + ks_kb + ks_lang + ks_tz + \
        ks_authconfig + ks_rootpw + ks_security + ks_bootloader + ks_disk_clear + \
        ks_part + ks_packages + ks_end + ks_post + ks_sched + ks_end

    return kickstart, tree

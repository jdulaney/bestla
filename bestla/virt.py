#! /usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# bestla -- Demonstrate easily spinning up virtual machines using web ui
#
"""virt.py  Objects for handling virtual machines"""

import os
import threading
from bestla.config import image_location, iface, subnet, dns
from bestla.ip import get_ip
from bestla.models import VirtualMachine, MasterImage
from tempfile import mkdtemp, mkstemp
import shutil
from time import sleep
import subprocess
from bestla.kickstart import kickstart_c7
from bestla import db


def set_status(name, status):
    """Pop vm from list, set status, return to list"""
    vm = VirtualMachine.query.filter_by(name=name).first()
    vm.status = status
    db.session.commit()
    return 0


def get_libvirt_status(name):
    """Get vm status from libvirt"""
    virt_states = subprocess.check_output(['virsh', 'list',
                                           '--all']).decode("utf-8", "ignore")
    states = virt_states.split('\n')
    for line in states:
        if name in line:
            items = filter(None, line.split(' '))
            if len(items) == 4:
                return items[2] + ' ' + items[3]
            else:
                return items[2]
    return 0


def vm_command(name, command):
    """Send commands to libvirt"""
    if command == 'Delete':
        vm_delete = DeleteVM(name)
        vm_delete.start()
    else:
        vm_action = VMWork(name, command)
        vm_action.start()
    return 0


def pxe_install(name, num_cpus, ram, disk, size):
    """Installs master image from kickstart"""
    ks_dir = mkdtemp()

    ks = ks_dir + '/anaconda-ks.cfg'
    ks_out, tree = kickstart_c7(subnet)
    ks_f = open(ks, 'w')
    ks_f.write(ks_out)
    ks_f.close()

    os.system(
        'virt-install' + ' --connect' + ' qemu:///session' + ' --nographics' +
        ' --name=' + name + ' -r=' + str(ram) + ' --vcpus=' + str(num_cpus) +
        ' -w ' + 'bridge=' + iface + ' --disk' + ' path=' + disk +
        ',format=qcow2,size=' + str(size) + ' -l=' + tree + ' --initrd-inject='
        + ks +
        ' --extra-args="ks=file:/anaconda-ks.cfg elevator=noop ksdevice=link console=ttyS0"'
        + ' --channel=none' + ' --noautoconsole')
    shutil.rmtree(os.path.dirname(ks))
    return 0


def vm_spinup(name, num_cpus, ram, disk, ssh_key, master_image, ip):
    """Copy image and then install it"""
    print disk, master_image
    subprocess.call(['qemu-img', 'create', '-b', master_image, '-f', 'qcow2',
                     disk])
    # Set IP and add ssh key:
    network = 'DEVICE=eth0\n' + \
        'IPADDR=' + ip + '\n' + \
        'GATEWAY=' + subnet + '\n' + \
        'BOOTPROTO=static\n' + \
        'NAME=eth0\n' + \
        'DNS1=' + dns + '\n' + \
        'ONBOOT=yes\n' + \
        'PREFIX=24\n'
    eth0_file = mkstemp()[1]
    f_eth0 = open(eth0_file, 'w')
    f_eth0.write(network)
    f_eth0.close()
    subprocess.call(['virt-sysprep', '-a', disk, '--hostname', name,
                     '--upload',
                     eth0_file + ':/etc/sysconfig/network-scripts/ifcfg-eth0'])
    subprocess.call(['virt-customize', '-a', disk, '--selinux-relabel',
                     '--ssh-inject', 'root:string:' + ssh_key])
    os.remove(eth0_file)

    os.system('virt-install' + ' --connect=qemu:///session' + ' --nographics' +
              ' --noautoconsole' + ' --import' + ' -w' + ' bridge=virbr0' +
              ' --name=' + name + ' -r=' + str(ram) + ' --vcpus=' +
              str(num_cpus) + ' --disk' + ' path=' + disk)


def master_install(os=None, image_import=False, path=None):
    """Install master image"""
    if image_import is False:
        name = os.strip(' ')
        image = image_location + name + '.img'
        image_install = InstallThread(name, '1', '1024', '0', image)
        image_install.start()
    else:
        image = path
    return image


def virt_install(vm_name):
    """Install new virtual machine"""
    vm = VirtualMachine.query.filter_by(name=vm_name).first()
    vm.status = 'Installing'
    vm.disk = image_location + vm_name + '.img'
    vm.ip = get_ip()
    db.session.commit()

    vm_install = InstallThread(vm_name, vm.num_cpus, vm.ram, vm.ip, vm.disk,
                               ssh_key=vm.ssh_key,
                               image=vm.master_image,
                               method='image')
    vm_install.start()
    return 0


class InstallThread(threading.Thread):
    """Handle installations"""

    def __init__(self, name, num_cpus, ram, ip, disk,
                 ssh_key=None,
                 image=None,
                 method='pxe'):
        threading.Thread.__init__(self)
        self.name = name
        self.num_cpus = num_cpus
        self.ram = ram
        self.disk = disk
        self.image = image
        self.method = method
        self.ip = ip
        self.size = '10'
        self.ssh_key = ssh_key

    def run(self):
        if self.method == 'pxe':
            pxe_install(self.name, self.num_cpus, self.ram, self.disk,
                        self.size)
        elif self.method == 'image':
            vm_spinup(self.name, self.num_cpus, self.ram, self.disk,
                      self.ssh_key, self.image, self.ip)

        if self.method == 'pxe':
            # We actually only need the vm image.
            subprocess.call(['virsh', 'undefine', self.name])
        else:
            set_status(self.name, 'running')
        return 0


class VMStatusPoll(threading.Thread):
    """Update vm status"""

    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while True:
            vm_list = VirtualMachine.query.all()
            for vm in vm_list:
                status = get_libvirt_status(vm.name)
                if vm.status != 'Installing':
                    set_status(vm.name, status)
            sleep(30)
        return 0


class VMWork(threading.Thread):
    """Modify VM status"""

    def __init__(self, name, command):
        threading.Thread.__init__(self)
        self.name = name
        self.command = command

    def run(self):
        """Execute vm status change"""
        commands = {
            'Start': ['start', 'started', 'running'],
            'Stop': ['destroy', 'destroyed', 'shut off'],
            'Pause': ['suspend', 'suspended', 'paused'],
            'Resume': ['resume', 'resumed', 'running']
        }
        command = commands.get(self.command)
        is_good = subprocess.check_output(['virsh', command[0], self.name])
        if command[1] in is_good:
            set_status(self.name, command[2])
        return 0


class DeleteVM(threading.Thread):
    """Delete machine"""

    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Execute deletion"""
        vm = VirtualMachine.query.filter_by(name=self.name).first()
        is_good = subprocess.check_output(['virsh', 'destroy', self.name])
        if ('destroyed' in is_good) or ('not running' in is_good):
            set_status(self.name, 'deleting')
            is_good = subprocess.check_output(['virsh', 'undefine', self.name])
            os.remove(vm.disk)
            db.session.delete(vm)
            db.session.commit()
        return 0




status_poll = VMStatusPoll()
status_poll.daemon = True

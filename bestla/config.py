#!/usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

import sys

""" Load configparser with backwards compatability for python 2 or 3
Python 3 uses configparser rather than ConfigParser """

if sys.version_info < (3, 0):
    import ConfigParser
    config = ConfigParser.SafeConfigParser({'BindIP4': '0.0.0.0',
                                            'BindPort': '5000'})
else:
    import configparser
    config = configparser.SafeConfigParser({'BindIP4': '0.0.0.0',
                                            'BindPort': '5000'})

config.read('etc/bestla.cfg')

listen_port = config.get("DEFAULT", "BindPort")
listen_ip = config.get("DEFAULT", "BindIP4")

from os.path import expanduser

home = expanduser(config.get("Virt", "Home"))
image_location = expanduser(config.get("Virt", "ImageLocation")) + '/'

iface = config.get("Virt", "BridgeInterface")

subnet = '192.168.122.1'
gateway = config.get("Virt", "Gateway")

dns = '192.168.122.1'

db_uri = 'sqlite:///' + home + '/bestla/test.db'

db_repository = home + '/bestla/db_repository'

#!/usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
# Malcolm VanOrder <mvanorder1390@gmail.com>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.


from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from config import db_uri, db_repository

webui = Flask(__name__)
webui.config['SECRET_KEY'] = 'hard to guess string'
webui.config['SQLALCHEMY_DATABASE_URI'] = db_uri
webui.config['SQLALCHEMY_MIGRATE_REPO'] = db_repository
db = SQLAlchemy(webui)

login_manager = LoginManager()
login_manager.init_app(webui)


@login_manager.user_loader
def load_user(userid):
    return User.get(userid)

from bestla.models import User, Role
from flask import render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.security import Security, SQLAlchemyUserDatastore
from bestla.models import roles_users

# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(webui, user_datastore)

# Create a user to test with


def create_user():
    user_datastore.create_user(
        username="admin", email='no@email.com', password='password', first_name='John', last_name='Doe')
    db.session.commit()
""" Disabled as create_user isn't working at the moment. It's use
it to create the first user and once working needs to be moved """
# create_user()

""" Not sure what this bit of code was for -- mvanorder -- 2015-05-10 """
# Views
#@webui.route('/')
#@login_required
# def home():
#        return render_template('index.html')
# if __name__ == '__main__':
#        app.run()

from bestla import views

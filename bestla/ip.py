#!/usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# Handle IP addresses for virtual machines

from bestla.config import iface, subnet
import subprocess
import string
from random import randint


def in_use_ips():
    """Check for currently in-use IPs on the current subnet"""
    arp_out = subprocess.check_output(['arp', '-n', '-i' + iface],
                                      universal_newlines=True)
    arp_out = arp_out.split('\n')
    arp_out.pop(0)  # Remove arp's headers
    arp_out.pop()  # Remove trailing newline
    ips = []
    for ip in arp_out:
        ip = ip.split(' ').pop(0)
        ips.append(ip)
    return ips


def generate_ip():
    """Generate a new ip within the subnet and ensure it
        does not conflict with in-use ips"""
    used_ips = in_use_ips()
    net = subnet.split('.')
    while True:
        num = randint(2, 254)
        ip = str(net[0]) + '.' + str(net[1]) + '.' + \
            str(net[2]) + '.' + str(num)
        if ip not in used_ips:
            break
    return ip


def get_ip():
    """Publicly facing function as ip assignment will likely change"""
    ip = generate_ip()
    return ip

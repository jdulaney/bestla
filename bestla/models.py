#!/usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
# Malcolm B. VanOrder <mvanorder1390@gmail.com>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.

from bestla import db
#from flask.ext.login import UserMixin
from flask import Flask, render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required

"""class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(212))
    email_address = db.Column(db.String(120), unique=True)
    first_name = db.Column(db.String(32))
    last_name = db.Column(db.String(32))
        
    def __repr__(self):
        return '<User %r>' % (self.username)
"""
class Logins(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    timestamp = db.Column(db.DateTime)
    success = db.Column(db.Boolean)
    source_ip = db.Column(db.String(15))

    def __repr__(self):
        return '<timestamp %r>' % (self.timestamp)


roles_users = db.Table('roles_users',
                       db.Column(
                           'user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(212))
    email = db.Column(db.String(128), unique=True)
    active = db.Column(db.Boolean())
    first_name = db.Column(db.String(32))
    last_name = db.Column(db.String(32))
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))


class VirtualMachine(db.Model):
    """Define a virtual machine"""
    id = db.Column(db.Integer, unique=True, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    users_name = db.Column(db.String(80))
    user = db.Column(db.String(80))
    ssh_key = db.Column(db.String(900))
    status = db.Column(db.String(80))
    disk = db.Column(db.String(80), unique=True)
    ram = db.Column(db.Integer)
    num_cpus = db.Column(db.Integer)
    ip = db.Column(db.String(80))
    creation_time = db.Column(db.Float(80))
    master_image = db.Column(db.String(80))

    def __repr__(self):
        return '<OS %r>' % (self.name)

class MasterImage(db.Model):
    """Create master image"""
    id = db.Column(db.Integer, unique=True, primary_key=True)
    os = db.Column(db.String(80), unique=True)
    creation_time = db.Column(db.Float(80))
    image = db.Column(db.String(80), unique=True)
    arch = db.Column(db.String(80))

    def __repr__(self):
        return '<OS %r>' % (self.os)




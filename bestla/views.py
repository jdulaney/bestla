#!/usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
# Malcolm B. VanOrder <mvanorder1390@gmail.com>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
"""Flask views"""

import sys
from time import time
from flask import render_template, flash, redirect, url_for
from bestla import webui, db
from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, TextAreaField, SelectField
from wtforms.validators import DataRequired
from bestla.virt import *
from flask.ext.security import login_required

ssh_keys = []


@webui.route('/')
@webui.route('/index')
#@login_required
def index():
    """Front page of site"""
    return render_template('index.html', title='Welcome to bestla')


@webui.route('/command/<name>/<command>')
@webui.route('/machines')
def machines(name=None, command=None):
    """Virtual machine management page"""
    vms = []
    vm_list = VirtualMachine.query.all()
    for vm in vm_list:
        start_stop_resume = ''
        pause = ''
        delete = ''
        if vm.status != 'installing':
            delete = 'Delete'
        if vm.status == 'running':
            start_stop_resume = 'Stop'
            pause = 'Pause'
        elif vm.status == 'shut off':
            start_stop_resume = 'Start'
        elif vm.status == 'paused':
            start_stop_resume = 'Resume'
        if vm.status != 0:
            vms.append({
                'name': vm.name,
                'status': vm.status,
                'ip': vm.ip,
                'start_stop': start_stop_resume,
                'delete': delete,
                'pause': pause
            })
    if name is not None:
        vm_command(name, command)
        name = None
    return render_template('vms.html', title='Machines', vms=vms)


@webui.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        # login and validate the user...
        login_user(user)
        flash("Logged in successfully.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("login.html", form=form)

@webui.route("/settings")
@login_required
def settings():
    pass

@webui.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(somewhere)


@webui.route('/master', methods=['GET', 'POST'])
def master_vm():
    master_form = MasterForm()
    if master_form.validate_on_submit():
        image_os = master_form.os.data
        path = master_install(os=image_os)
        new_image = MasterImage(
            os=image_os, creation_time=time(), image=path, arch='x86_64')
        #master_images.append(new_image)
        # new_image.install()
        db.session.add(new_image)
        db.session.commit()
        return redirect('/master')
    return render_template('master.html',
                           title='Master Images', form=master_form)


@webui.route('/import', methods=['GET', 'POST'])
def import_image():
    import_form = ImportForm()
    if import_form.validate_on_submit():
        image_path = import_form.path.data
        image_os = import_form.os.data
        path = master_install(image_import=True, path=image_path)
        new_image = MasterImage(
            os=image_os, creation_time=time(), image=path, arch='x86_64')
        #master_images.append(new_image)
        db.session.add(new_image)
        db.session.commit()
        return redirect('/')
    return render_template('import.html',
                           title='Import Image', form=import_form)


@webui.route('/new', methods=['GET', 'POST'])
def new():
    virt_form = VirtForm()
    disable = ['', '']
    master_images = MasterImage.query.all()
    if master_images == []:
        disable = ['disabled',
                   'Unfortunaetly, there are no currently configured master images']
    elif ssh_keys == []:
        disable = ['disabled',
                   'Please enter an SSH key so that you can access your machine.']
    else:
        virt_form.key.choices = [(ssh_key['key'], ssh_key['name']) for ssh_key
                                 in ssh_keys]
        virt_form.os.choices = [(image.image, image.os)
                                for image in master_images]

    if virt_form.validate_on_submit() and (disable[0] == ''):
        user = 'test'
        vm_name = virt_form.name.data
        vm_image = virt_form.os.data
        users_name = vm_name
        vm_name = user + '_' + users_name
        ssh_key = virt_form.key.data
        new_vm = VirtualMachine(
            name=vm_name, users_name=users_name, user=user, ssh_key=ssh_key,
                                status='Installing', disk='', ram=1024, num_cpus=1, ip='', creation_time=time(), master_image=vm_image)
        db.session.add(new_vm)
        db.session.commit()
        virt_install(vm_name)
        return redirect('/index')
    return render_template('virtform.html', title='Create Virtual Machine',
                           form=virt_form, disable=disable)


@webui.route('/key_action/<name>/<command>')
@webui.route('/ssh')
#@login_required
def ssh(name=None, command=None):
    """ssh key management page"""
    if command == 'delete':
        for key in ssh_keys:
            if key['name'] == name:
                ssh_keys.pop(ssh_keys.index(key))
    return render_template('ssh.html', title='SSH Key Management',
                           keys=ssh_keys)


@webui.route('/new_ssh', methods=['GET', 'POST'])
def new_ssh():
    ssh_form = sshForm()
    name = ''
    if ssh_form.validate_on_submit():
        name = ssh_form.name.data
        key = ssh_form.key.data
        ssh_keys.append({'name': name, 'key': key})
        return redirect('/ssh')
    return render_template('new_ssh.html', title='Paste in SSH Public Key',
                           form=ssh_form)


class ImportForm(Form):
    path = StringField('path', validators=[DataRequired()])
    os = StringField('os', validators=[DataRequired()])


class MasterForm(Form):
    name = StringField('name', validators=[DataRequired()])
    os = SelectField('os', choices=[('cent7.0', 'Centos 7.0')])


class sshForm(Form):
    name = StringField('name', validators=[DataRequired()])
    key = TextAreaField('key', validators=[DataRequired()])


class VirtForm(Form):
    name = StringField('name', validators=[DataRequired()])
    os = SelectField('os')
    key = SelectField('key')

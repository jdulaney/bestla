#Bestla

A proof of concept cloud by John H. Dulaney. `<jdulaney@fedoraproject.org>`

###Install

* `git clone https://bitbucket.org/jdulaney/bestla/`, then install the deps.
* create a folder in your home directory called `bestla` for VM image
storage.

###Dependencies
* python-flask
* python-flask-wtf
* python-flask-sqlalchemy
* python-flask-principal
* python-passlib
* sqlalchemy-migrate
* python-migrate
* libvirt
* qemu-kvm
* qemu-img
* libguestfs-tools
* python-pip
* flask-security from pip

*Note, these are Fedora package names; if you use another $linux, your package
names may vary.*

###Configuration

Poke around in `bestla/config.py`


###Running

Make sure you have an Internet connection, as right now I'm installing VMs
by essentially PXE booting. This allows me to ship bestla self-contained
without having to point to filesystem images. Right now it just installs
CentOS 7.

cd into the dir where you git cloned to, and type `./bestla.py`

Flask will print out a helpful link that you can copy and paste into your
browser.

Have fun!


John.
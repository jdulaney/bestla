#! /usr/bin/python

import db
db.create_db()
db.create_migration()
db.upgrade_db()

#!/usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
# Malcolm B. VanOrder <mvanorder1390@gmail.com>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
"""Functions to control the database"""

from flask.ext.sqlalchemy import SQLAlchemy
from migrate.versioning import api
import os.path
import imp
from bestla import db
from bestla.config import db_uri, db_repository
import argparse

def create_db():
    """Initialize database"""
    db.create_all()
    if not os.path.exists(db_repository):
        api.create(db_repository, 'database repository')
        api.version_control(db_uri, db_repository)
    else:
        api.version_control(db_uri, db_repository, api.version(db_repository))
    return 0


def create_migration():
    """Create database migrations"""
    version = api.db_version(db_uri, db_repository)
    migration = db_repository + ('/versions/%03d_migration.py' % (version + 1))
    tmp_module = imp.new_module('old_model')
    old_model = api.create_model(db_uri, db_repository)
    exec(old_model, tmp_module.__dict__)
    script = api.make_update_script_for_model(
        db_uri, db_repository, tmp_module.meta, db.metadata)
    migration_f = open(migration, "wt")
    migration_f.write(script)
    migration_f.close()
    return 'New migration saved as ' + migration


def upgrade_db():
    api.upgrade(db_uri, db_repository)
    version = api.db_version(db_uri, db_repository)
    return 'Current database version: ' + str(version)


def downgrade_db():
    version = api.db_version(db_uri, db_repository)
    api.downgrade(db_uri, db_repository, version - 1)
    version = api.db_version(db_uri, db_repository)
    return 'Current database version: ' + str(version)

if __name__ == '__main__':
    create_migration()
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--create", action="store_true",
            help="Create database")
    parser.add_argument("-m", "--migrate", action="store_true",
            help="Create new database migration")
    parser.add_argument("-u", "--upgrade", action="store_true",
            help="Upgrade database")
    parser.add_argument("-d", "--downgrade", action="store_true",
            help="Downgrade database")
    args = parser.parse_args()
    if args.upgrade & args.downgrade:
        print "Can't upgrade and downgrade the database at the same time\n"
    else:
        if args.create:
            create_db()
        if args.migrate:
            create_migration()
        if args.upgrade:
            upgrade_db()
        else:
            if args.downgrade:
                downgrade_db()

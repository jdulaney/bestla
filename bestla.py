#! /usr/bin/python
from bestla.virt import status_poll
from bestla import webui
from bestla import config
import sys
import socket

# Check if IP defined in the config is a valid IP to bind to
try:
    socket.inet_aton(config.listen_ip)
except socket.error:
    print('Address is invalid for IPv4', config.listen_ip)
    print('Falling back to defualt IPv4.')
    config.listen_ip = '0.0.0.0'


# Check if socket is available to bind to
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.bind((config.listen_ip, int(config.listen_port)))
except socket.error as msg:
    print('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
    sys.exit()
s.close()


if __name__ == '__main__':
    print('bestla')
    status_poll.start()
    webui.run(host=config.listen_ip,
              port=int(config.listen_port),
              debug=True)

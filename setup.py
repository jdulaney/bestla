#!/usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
# Malcolm B. VanOrder <mvanorder1390@gmail.com>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA

import md5
import os
import hashlib
from base64 import b64encode
from bestla import db, models

""" This isn't being used/maintaned at the moment. I'm attempting to
use flask-security's built in function to encrypt passwords and create
users in bestla/__init__.py """

salt = b64encode(os.urandom(24))
password = "Password"
hash_object = hashlib.sha512(salt + 'Hello World', 1000)
hex_dig = hash_object.hexdigest()
password_data = "sha512:" + salt + ":" + b64encode(hex_dig)
u = models.User(username='admin', password=password_data, first_name='John', last_name="Doe")
db.session.add(u)
db.session.commit()
